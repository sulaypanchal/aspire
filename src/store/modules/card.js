const actions = {
  addCard:({commit}, name) => {
    const card = {
       id: state.cards[state.cards.length -1].id + 1,
       name,
       card_number: (Math.floor(Math.random() * (9999999999999999 - 1111111111111111)) + 1111111111111111).toString(),
       exp_date: '09/22'
     }
     commit('saveCard',card);
   },

   freezeCard:({commit}, index) => {
     commit('changeCardStatus', index);
   },

   removeCard:({commit}, index) => {
    commit('deleteCard', index);
   }
};

const getters = {
  allCards:(state) => state.cards,
};

const mutations = {
  saveCard:(state, card) => {
    state.cards.push(card);
  },
  changeCardStatus:(state, index) => {
    state.cards[index].freeze = !state.cards[index].freeze;
  },
  deleteCard:(state, index) => {
    state.cards.splice(index, 1);
  }
};

const state = {
  cards:[
    {
      id: 1,
      name: "Panchal Sulay",
      card_number: "4111111111111111",
      exp_date:"08/22",
      freeze: true,
    },
    {
      id: 2,
      name: "Thanh Nghiem",
      card_number: "4242424242424242",
      exp_date:"09/32",
      freeze: false,
    }
  ]
};

export default {
  actions,
  getters,
  mutations,
  state,
}