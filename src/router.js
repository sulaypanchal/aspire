//import Layout from './components/Layout.vue';
import Cards from './components/pages/cards.vue';
import Home from './components/pages/home.vue';

const router = [
  {
    path: "/",
    component: Cards,
  },
  {
    path: "/home",
    component: Home,
  }
];

export default router;
